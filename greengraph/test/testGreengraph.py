import yaml
import os
import numpy as np
import unittest
from ..greengraph import Greengraph
from nose.tools import *
import geopy
from mock import Mock, patch


class TestGreengraph(unittest.TestCase):

    def setUp(self):
        with open(os.path.join(os.path.dirname(__file__),'fixtures','samples.yaml')) as fixture_file:
            self.fixtures = yaml.load(fixture_file)
    
    def test_geolocate(self):
        for fix in self.fixtures['geolocate']:
            location = fix.pop('location')
            anslat = fix.pop('anslat')
            anslong = fix.pop('anslong')
            answer = (anslat, anslong)
            #with patch.object(geopy.geocoders, 'GoogleV3') as mock_GoogleV3:
            #    geo = GreengraphHelper(1, 2)
            #    print geo.geolocate(location)
            geo_attr = {'geocoder.geocode.return_value' : answer}
            geo = Mock(**geo_attr)
            assert_equal(geo.geocoder.geocode(location), answer)


    def test_location_sequence(self):
        pass

    def test_green_between(self):
        pass

if __name__ == '__main__':
    unittest.main()

